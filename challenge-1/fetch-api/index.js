//Install dependencies with your package manager of choice before running this
//this file from your terminal with node.
const fetch = require("node-fetch");
const https = require("https");

const apiURL = "https://worldclockapi.com/";
//Since this api doesn't have a reference of supported timezones and there isn't
//a clear consensus on "registered timezones", I pulled these from wikipedia.
//Responses are trial and error based.
const timezones = [
  'DST', 'U', 'HST', 'AKDT', 'PDT', 'PDT', 'PST',
  'UMST', 'MDT', 'MDT', 'CAST', 'CDT', 'CDT', 'CCST',
  'SPST', 'EDT', 'UEDT', 'VST', 'PYT', 'ADT', 'CBST',
  'SWST', 'PSST', 'NDT', 'ESAST', 'AST', 'SEST', 'GDT',
  'MST', 'BST', 'U', 'MDT', 'ADT', 'CVST', 'MDT',
  'UTC', 'GMT', 'BST', 'GDT', 'GST', 'WEDT', 'CEDT',
  'RDT', 'CEDT', 'WCAST', 'NST', 'GDT', 'MEDT', 'EST',
  'SDT', 'EEDT', 'SAST', 'FDT', 'TDT', 'JDT', 'LST',
  'JST', 'AST', 'KST', 'AST', 'EAST', 'MSK', 'SAMT',
  'IDT', 'AST', 'ADT', 'MST', 'GET', 'CST', 'AST',
  'WAST', 'YEKT', 'PKT', 'IST', 'SLST', 'NST', 'CAST',
  'BST', 'MST', 'SAST', 'NCAST', 'CST', 'NAST', 'MPST',
  'WAST', 'TST', 'UST', 'NAEST', 'JST', 'KST', 'CAST',
  'ACST', 'EAST', 'AEST', 'WPST', 'TST', 'YST', 'CPST',
  'VST', 'NZST', 'SST', 'TST', 'KDT', 'MST', 'U', 'NZST'
]

//Worldclockapi hostname doesn't match altnames at the time of writing, never let
//this slide in production, tons of security issues.
const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
});

for (i in timezones) {
  const abbr = timezones[i];
  const requestURL = `${apiURL}/api/json/${abbr}/now`;
  fetch(requestURL, { agent: httpsAgent })
    .then(response => response.json()
      .then(data => {
        //Don't log unregistered timezones.
        if (data.serviceResponse === null) {
          console.log(`${abbr}:`, data, "\n");
        }
      })
    );
}