# Trustly technical challenge - System Integration Engineer

## Challenge 1
*The challenge is to find a way to use the API (http://worldclockapi.com/)*

### Requirements
* You need to display the results on screen.
* You need to use JavaScript or related programing language (i.e. NodeJS, ReactJS and others).
* We must understand your code and use your API without having to ask you any questions. Our primary language is English so please use it on comments and documentation.
* We’d like to see SOLID principles in your solution.
* You don’t need to persist any data (but feel free to do it if you want).

### Solution
* Solution 1:
	Makes several requests using the javascript fetch API. From your terminal, cd into `/challenge-1/fetch-api` and run `node index.js`.

* Solution 2:
	A simple website using Vue and axios that makes requests to the appropriate endpoint on user input.
	
## Challenge 2
*Create a Java class to print your name, nationality and biggest dream.*

### Requirements
* You need to display the results on screen.
* You need to use Java.
* You don’t need to persist any data (but feel free to do it if you want).

### Solution
 * Very simple IO with a Java scanner 