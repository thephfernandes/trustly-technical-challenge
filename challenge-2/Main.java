import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("What's your full name?");
        String name = input.nextLine();
        System.out.println("Nice to meet you, " + name + ". Where are you from?");
        String nationality = input.nextLine();
        System.out.println(name + " is from " + nationality + ". What's your biggest dream?");
        String dream = input.nextLine();
        System.out.println(dream + "... that's an awesome dream!");
    }
}